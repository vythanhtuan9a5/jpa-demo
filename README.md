# jpa-demo
This project was created for demo how to interactive with entity in database.
## Prerequisite
+ java 11
+ docker
## Installation
+ step 1: install postgre db
``
docker run --name postgresql -p 5432:5432 -e POSTGRESQL_PASSWORD=password bitnami/postgresql:latest
``
+ step 2: build project by gradle
```
./gradlew clean build -x test
```
