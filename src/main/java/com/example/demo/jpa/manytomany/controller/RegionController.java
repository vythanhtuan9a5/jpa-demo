package com.example.demo.jpa.manytomany.controller;

import com.example.demo.jpa.manytomany.entity.Region;
import com.example.demo.jpa.manytomany.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/region")
public class RegionController {
    private final RegionRepository regionRepository;

    @GetMapping
    public ResponseEntity getAll() {
        return ResponseEntity.ok(regionRepository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(regionRepository.getById(id));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Region region) {
        return ResponseEntity.ok(regionRepository.save(region));
    }

    @PutMapping("{id}")
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Region region) {
        Region inDB = regionRepository.getById(id);
        BeanUtils.copyProperties(region, inDB);
        inDB.setId(id);
        return ResponseEntity.ok(regionRepository.save(inDB));
    }

}
