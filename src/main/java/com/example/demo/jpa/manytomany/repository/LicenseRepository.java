package com.example.demo.jpa.manytomany.repository;

import com.example.demo.jpa.manytomany.entity.License;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LicenseRepository extends JpaRepository<License, Long> {
}
