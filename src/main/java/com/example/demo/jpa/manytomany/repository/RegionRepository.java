package com.example.demo.jpa.manytomany.repository;

import com.example.demo.jpa.manytomany.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region, Long> {
}
