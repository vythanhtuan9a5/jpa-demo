package com.example.demo.jpa.manytomany.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Log4j2
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class License {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "license_id")
    @ToString.Exclude
    private Set<LicenseRegion> licenseRegions = new HashSet<>();

    @PostPersist
    public void postPersist() {
        log.info("create success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostRemove
    public void postRemove() {
        log.info("remove success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostUpdate
    public void postUpdate() {
        log.info("change success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        License license = (License) o;
        return id != null && Objects.equals(id, license.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
