package com.example.demo.jpa.manytomany.controller;

import com.example.demo.jpa.manytomany.entity.License;
import com.example.demo.jpa.manytomany.entity.LicenseRegion;
import com.example.demo.jpa.manytomany.entity.Region;
import com.example.demo.jpa.manytomany.repository.LicenseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Set;

@Transactional
@RequiredArgsConstructor
@RestController
@RequestMapping("api/license")
public class LicenseController {
    private final LicenseRepository repository;

    @GetMapping
    public ResponseEntity getAll() {
        return ResponseEntity.ok(repository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(repository.getById(id));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody License license) {
        Set<LicenseRegion> licenseRegions = license.getLicenseRegions();
        license.setLicenseRegions(new HashSet<>());
        License saveLicense = repository.save(license);
        licenseRegions.forEach(licenseRegion -> licenseRegion.setLicense(saveLicense));
        saveLicense.setLicenseRegions(licenseRegions);
        return ResponseEntity.ok(repository.save(saveLicense));
    }

    @PutMapping("{id}")
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody License license) {
        License inDB = repository.getById(id);
        inDB.setId(id);
        inDB.setName(license.getName());
        inDB.getLicenseRegions().clear();
        license.getLicenseRegions().forEach(licenseRegion -> licenseRegion.setLicense(inDB));
        inDB.getLicenseRegions().addAll(license.getLicenseRegions());
        return ResponseEntity.ok(repository.save(inDB));
    }

    @PostMapping("{id}/region")
    public ResponseEntity addRegionToLicense(@PathVariable("id") Long id, @RequestBody Region region) {
        License inDB = repository.getById(id);
        LicenseRegion licenseRegion = new LicenseRegion();
        licenseRegion.setRegion(region);
        inDB.getLicenseRegions().add(licenseRegion);
        return ResponseEntity.ok(repository.save(inDB));
    }

    @PutMapping("{id}/region/{region_id}")
    public ResponseEntity editLicenseRegion(@PathVariable("id") Long id, @PathVariable("region_id") Long regionId, @RequestBody LicenseRegion licenseRegion) {
        License inDB = repository.getById(id);
        LicenseRegion licenseRegion1 = inDB.getLicenseRegions().stream().filter(licenseRegion2 -> licenseRegion2.getRegion().getId().equals(regionId)).findFirst().orElseThrow(EntityNotFoundException::new);
        licenseRegion1.setActivated(licenseRegion.getActivated());
        return ResponseEntity.ok(repository.save(inDB));
    }

}
