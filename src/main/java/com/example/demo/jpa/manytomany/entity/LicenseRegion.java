package com.example.demo.jpa.manytomany.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Log4j2
public class LicenseRegion {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinColumn(name = "license_id")
    @Hidden
    @JsonIgnore
    private License license;
    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;
    @Column
    private Boolean activated = Boolean.TRUE;

    @PostPersist
    public void postPersist() {
        log.info("create success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostRemove
    public void postRemove() {
        log.info("remove success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostUpdate
    public void postUpdate() {
        log.info("change success class {} id {}", this.getClass().getSimpleName(), id);
    }

}
