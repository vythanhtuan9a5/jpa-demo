package com.example.demo.jpa.onetoone.case2.controller;

import com.example.demo.jpa.onetoone.case2.entity.User;
import com.example.demo.jpa.onetoone.case2.entity.UserDetail;
import com.example.demo.jpa.onetoone.case2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/user")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @GetMapping
    public List<User> getall() {
        return userRepository.findAll();
    }

    @PostMapping
    public User create(@RequestBody User user) {
        if(user.getUserDetail() == null) user.setUserDetail( new UserDetail());
        return userRepository.save(user);
    }

    @PutMapping("{id}")
    public User update(@PathVariable("id") Long userId, @RequestBody User user) {
        User inDB = userRepository.getById(userId);
        inDB.setUsername(user.getUsername());
        inDB.setPassword(user.getPassword());
        return userRepository.save(user);
    }

    @PutMapping("{id}/detail")
    public User updateDetail(@PathVariable("id") Long userId, @RequestBody UserDetail user) {
        User inDB = userRepository.getById(userId);
        UserDetail userDetail = inDB.getUserDetail();
        userDetail.setFullName(user.getFullName());
        userDetail.setPhone(user.getPhone());
        userDetail.setAddress(user.getAddress());
        inDB.setUserDetail(userDetail);
        return userRepository.save(inDB);
    }
}
