package com.example.demo.jpa.onetoone.case1.controller;

import com.example.demo.jpa.onetoone.case1.entity.Body;
import com.example.demo.jpa.onetoone.case1.entity.Head;
import com.example.demo.jpa.onetoone.case1.entity.Robot;
import com.example.demo.jpa.onetoone.case1.repository.RobotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/factory/robot")
public class RobotController {
    @Autowired
    RobotRepository repository;

    @GetMapping
    public List<Robot> getAll() {
        return repository.findAll();
    }

    @PostMapping
    public Robot create(@RequestBody Robot robot) {
        robot = repository.save(robot);
        robot.getHead().setRobot(robot);
        robot.getBody().setRobot(robot);
        return repository.save(robot);
    }

    @PutMapping("{id}")
    public Robot update(@PathVariable("id") Long robotId, @RequestBody Robot robot) {
        Robot inDB = repository.getById(robotId);
        inDB.setName(robot.getName());
        robot.getBody().setRobot(inDB);
        inDB.setBody(robot.getBody());
        robot.getBody().setRobot(inDB);
        inDB.setHead(robot.getHead());
        return repository.save(inDB);
    }
}
