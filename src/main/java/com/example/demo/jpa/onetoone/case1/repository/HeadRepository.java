package com.example.demo.jpa.onetoone.case1.repository;

import com.example.demo.jpa.onetoone.case1.entity.Head;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface HeadRepository extends JpaRepository<Head, Long> {
}
