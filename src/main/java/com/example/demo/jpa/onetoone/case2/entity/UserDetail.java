package com.example.demo.jpa.onetoone.case2.entity;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;

@Entity
@Data
@Log4j2
public class UserDetail {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String fullName;
    @Column
    private String address;
    @Column
    private String phone;

    @JoinColumn(name = "user_id")
    @OneToOne
    private User user;


    @PostPersist
    public void postPersist() {
        log.info("create success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostRemove
    public void postRemove() {
        log.info("remove success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostUpdate
    public void postUpdate() {
        log.info("change success class {} id {}", this.getClass().getSimpleName(), id);
    }

}
