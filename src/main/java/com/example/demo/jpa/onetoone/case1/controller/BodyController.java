package com.example.demo.jpa.onetoone.case1.controller;

import com.example.demo.jpa.onetoone.case1.entity.Body;
import com.example.demo.jpa.onetoone.case1.repository.BodyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/factory/body")
public class BodyController {
    @Autowired
    BodyRepository headRepository;

    @GetMapping
    public List<Body> getAll() {
        return headRepository.findAll();
    }

    @PostMapping
    public Body create(@RequestBody Body Body) {
        return headRepository.save(Body);
    }
}
