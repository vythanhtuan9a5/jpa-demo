package com.example.demo.jpa.onetoone.case1.controller;

import com.example.demo.jpa.onetoone.case1.entity.Head;
import com.example.demo.jpa.onetoone.case1.repository.HeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/factory/head")
public class HeadController {
    @Autowired
    HeadRepository headRepository;

    @GetMapping
    public List<Head> getAll() {
        return headRepository.findAll();
    }

    @PostMapping
    public Head create(@RequestBody Head head) {
        return headRepository.save(head);
    }
}
