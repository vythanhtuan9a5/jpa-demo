package com.example.demo.jpa.onetoone.case2.entity;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;

@Entity
@Data
@Log4j2
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String username;
    @Column
    private String password;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private UserDetail userDetail;


    @PostPersist
    public void postPersist() {
        log.info("create success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostRemove
    public void postRemove() {
        log.info("remove success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostUpdate
    public void postUpdate() {
        log.info("change success class {} id {}", this.getClass().getSimpleName(), id);
    }

}
