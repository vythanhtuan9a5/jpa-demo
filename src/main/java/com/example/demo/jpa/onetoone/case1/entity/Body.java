package com.example.demo.jpa.onetoone.case1.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@Log4j2
@NoArgsConstructor
public class Body {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String code;
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "robot_id")
    private Robot robot;

    @PostPersist
    public void postPersist() {
        log.info("create success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostRemove
    public void postRemove() {
        log.info("remove success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostUpdate
    public void postUpdate() {
        log.info("change success class {} id {}", this.getClass().getSimpleName(), id);
    }
}
