package com.example.demo.jpa.onetoone.case1.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;

@Entity
@Data
@Log4j2
@NoArgsConstructor
public class Robot {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "head_id",referencedColumnName = "id")
    private Head head;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "body_id",referencedColumnName = "id")
    private Body body;

    @PostPersist
    public void postPersist() {
        log.info("create success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostRemove
    public void postRemove() {
        log.info("remove success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostUpdate
    public void postUpdate() {
        log.info("change success class {} id {}", this.getClass().getSimpleName(), id);
    }
}
