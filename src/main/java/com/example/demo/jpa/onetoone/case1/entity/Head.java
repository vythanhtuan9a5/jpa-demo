package com.example.demo.jpa.onetoone.case1.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;

@Data
@Entity
@Log4j2
@NoArgsConstructor
public class Head {
    @Id
    @GeneratedValue
    private Long id;
    @OneToOne
    @JsonIgnore
    @JoinColumn(name = "robot_id")
    private Robot robot;
    @Column
    private String code;
    @PostPersist
    public void postPersist() {
        log.info("create success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostRemove
    public void postRemove() {
        log.info("remove success class {} id {}", this.getClass().getSimpleName(), id);
    }

    @PostUpdate
    public void postUpdate() {
        log.info("change success class {} id {}", this.getClass().getSimpleName(), id);
    }
}
