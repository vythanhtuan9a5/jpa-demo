package com.example.demo.jpa.onetomany.service;

import com.example.demo.jpa.onetomany.entity.Clazz;
import com.example.demo.jpa.onetomany.entity.School;
import com.example.demo.jpa.onetomany.repository.SchoolRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RequiredArgsConstructor
@Service
@Log4j2
@Transactional
public class SchoolService {
    private final SchoolRepository schoolRepository;

    public School create(School school) {
        //some condition
        return schoolRepository.save(school);
    }


    public Clazz updateClazz(Long schoolId, Long clazzId, Clazz updatedClazz) {
        School school = schoolRepository.getById(schoolId);
        Clazz clazz = school.getClazzes().stream().filter(clazz1 -> clazz1.getId().equals(clazzId)).findFirst().orElseThrow(EntityNotFoundException::new);
        clazz.setName(updatedClazz.getName());
        schoolRepository.save(school);
        return clazz;
    }

    public List<School> getAll() {
        return schoolRepository.findAll();
    }

    public School getById(Long uuid) {
        return schoolRepository.getById(uuid);
    }

    public School update(Long schoolId, School school) {
        //add some condition...
        School inDB = schoolRepository.getById(schoolId);
        inDB.setName(school.getName());
        inDB.getClazzes().clear();
        inDB.getClazzes().addAll(school.getClazzes());
        return schoolRepository.save(inDB);
    }
}
