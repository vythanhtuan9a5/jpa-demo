package com.example.demo.jpa.onetomany.controller;

import com.example.demo.jpa.onetomany.entity.Clazz;
import com.example.demo.jpa.onetomany.entity.School;
import com.example.demo.jpa.onetomany.service.SchoolService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/school")
public class SchoolController {
    private final SchoolService schoolService;

    @GetMapping("")
    public ResponseEntity<List<School>> getAll() {
        return ResponseEntity.ok(schoolService.getAll());
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<School> getById(@PathParam("id") Long uuid) {
        return ResponseEntity.ok(schoolService.getById(uuid));
    }

    @PostMapping
    public ResponseEntity<School> create(@RequestBody School school) {
        return ResponseEntity.ok(schoolService.create(school));
    }

    @PutMapping("{id}")
    public ResponseEntity<School> update(@PathVariable("id") Long schoolId, @RequestBody School school) {
        return ResponseEntity.ok(schoolService.update(schoolId, school));
    }

    @PutMapping("{id}/class/{class_id}")
    public ResponseEntity<Clazz> updateClassInSchool(@PathVariable("id") Long schoolId, @PathVariable("class_id") Long clazzId, Clazz clazz) {
        return ResponseEntity.ok(schoolService.updateClazz(schoolId, clazzId, clazz));
    }
}
