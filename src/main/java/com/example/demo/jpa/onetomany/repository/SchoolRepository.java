package com.example.demo.jpa.onetomany.repository;

import com.example.demo.jpa.onetomany.entity.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Repository
@Transactional
public interface SchoolRepository extends JpaRepository<School, Long> {
    School findByIdAndClazzes_Id(Long id, Long id1);

}
