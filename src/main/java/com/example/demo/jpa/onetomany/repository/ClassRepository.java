package com.example.demo.jpa.onetomany.repository;

import com.example.demo.jpa.onetomany.entity.Clazz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface ClassRepository extends JpaRepository<Clazz, Long> {
}
